/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptixiaki_jlabel;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import static ptixiaki_jlabel.MazeBoardPanel.STEPS;
import static ptixiaki_jlabel.MazeBoardPanel.TURNS;

/**
 *
 * @author std84266
 */
public class BarChart_AWT {

    private MazeBoardPanel mazeBoard;
    private int Time;

    public BarChart_AWT(String applicationTitle, String chartTitle) {
        //super(applicationTitle);

        //setContentPane(chartPanel);
    }

    public CategoryDataset createDataset() {
        final String fiat = "STEPS";
        final String audi = "TURNS";
        final String ford = "TIME";
        final String speed = "1";
        final String millage = "2";
        final String userrating = "3";
        final String safety = "4";
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();
        System.out.println("RUN JFRREE==" + this.getTime());

        dataset.addValue(this.getSteps(), fiat, speed);
        dataset.addValue(0.0, fiat, userrating);
        dataset.addValue(0.0, fiat, millage);
        dataset.addValue(0.0, fiat, safety);

        dataset.addValue(this.getTurns(), audi, speed);
        dataset.addValue(0.0, audi, userrating);
        dataset.addValue(0.0, audi, millage);
        dataset.addValue(0.0, audi, safety);

        dataset.addValue(this.getTime(), ford, speed);
        dataset.addValue(0.0, ford, userrating);
        dataset.addValue(0.0, ford, millage);
        dataset.addValue(0.0, ford, safety);

        return dataset;
    }

    public void setTime(int time) {
        Time = time;
    }

    public int getTime() {
        return Time;
    }

    public void setSteps(int steps) {
        STEPS = steps;
    }

    public int getSteps() {
        return STEPS;
    }

    public void setTurns(int turns) {
        TURNS = turns;
    }

    public int getTurns() {
        return TURNS;
    }

}
