/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptixiaki_jlabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static ptixiaki_jlabel.MazeBoardPanel.PREF_H;
import static ptixiaki_jlabel.MazeBoardPanel.PREF_W;

/**
 *
 * @author std84266
 */
public class ScreenFrame extends JFrame {

    public ScreenFrame() throws IOException {
        this.setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Maze Solver Simulation");

        JPanel panel = new JPanel();
        panel.setBackground(Color.black);
        panel.setVisible(true);
        panel.setLayout(new BorderLayout());
        panel.setPreferredSize(new Dimension(PREF_W, PREF_H));
        this.add(panel);

        MazeBoardPanel mazeBoard = new MazeBoardPanel();
        mazeBoard.createAndShowGui();

        panel.add(mazeBoard, BorderLayout.WEST);

        ControlPanel controlPanel = new ControlPanel();
        panel.add(controlPanel, BorderLayout.EAST);

        controlPanel.setMazeBoardPanel(mazeBoard);

        pack();

    }
}
