/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptixiaki_jlabel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import static ptixiaki_jlabel.MazeBoardPanel.N;

public class MazeBoardPanel extends JPanel implements Cloneable, Serializable {

    protected static int M = 33;
    protected static int N = 33;
    protected static final int PREF_W = 1500;
    protected static final int PREF_H = 1000;
    protected static final int START_X = 0;
    protected static final int START_Y = 0;

    private static ImageIcon iconDown = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\down.png");
    private static ImageIcon iconLeft = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\left.png");
    private static ImageIcon iconRight = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\right.png");
    private static ImageIcon iconUp = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\up.png");
    private static CellJlabel[][] cell = new CellJlabel[M][N];
    private static Stack stack = new Stack();
    private static Stack openstack = new Stack();
    protected static int TIME = 100;
    protected static int TURNS = 0;
    protected static int STEPS = 0;
    private static int G = 0;
    private static int F = 0;

    public MazeBoardPanel() {
        this.setVisible(true);
        this.setBackground(Color.black);
        this.setLayout(new GridLayout(M, N));
        this.setBorder(BorderFactory.createLineBorder(Color.black));
        this.setPreferredSize(new Dimension(PREF_W, PREF_H));

    }

    public void createAndShowGui() {

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (i == START_X && j == START_Y) {
                    cell[i][j] = new CellJlabel(i, j, calcDist(i, j));
                    cell[i][j].setBackground(Color.green);
                    // cell[i][j].setIcon(iconDown);
                    //cell[i][j].setHeading(1);
                    this.add(cell[i][j]);
                } else if (((i == M / 2 && j == N / 2))) {
                    cell[i][j] = new CellJlabel(i, j, calcDist(i, j));
                    cell[i][j].setBackground(Color.red);
                    cell[i][j].setIcon(null);
                    this.add(cell[i][j]);
                } else {
                    cell[i][j] = new CellJlabel(i, j, calcDist(i, j));
                    cell[i][j].setBackground(Color.white);
                    this.add(cell[i][j]);
                    addMouseMotionListener(cell[i][j]);
                    addMouseListener(cell[i][j]);
                }
            }
        }

    }

    public int calcDist(int x, int y) {
        int dist = Math.abs(N / 2 - y) + Math.abs(M / 2 - x);
        //System.out.println("X=" + x + " Y=" + y + " Value=" + dist);
        return dist;
    }

    public void reCalcDist() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (cell[i][j].getValue() != M * N) {
                    cell[i][j].setValue(this.calcDist(i, j));
                    cell[i][j].setText(Integer.toString(cell[i][j].getValue()));
                }
            }
        }
    }

    public void fillFlood(int x, int y) throws InterruptedException {

        int newX = 0;
        int newY = 0;

        CellJlabel cellmin = findMinCell(x, y);

        if (cell[x][y].getValue() == cellmin.getValue() + 1 && cell[x][y].getValue() != 255) {
            SwingWorker worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    TimeUnit.MILLISECONDS.sleep(TIME * 4);
                    cellmin.repaintCell(cellmin, cell[x][y]);
                    return null;
                }
            };
            worker.execute();
            newX = cellmin.getCellX();
            newY = cellmin.getCellY();

            cellmin.setHeading(orientation(x, y, newX, newY));

            if (cellmin.getHeading() != cell[x][y].getHeading()) {
                setTurns(getTurns() + 1);
            }
            setSteps(getSteps() + 1);

        } else {
            fillFloodNew(x, y);
            newX = x;
            newY = y;

        }

        if ((newX == M / 2 && newY == N / 2)) {
            cell[newX][newY].setBackground(Color.red);
            cell[M / 2][N / 2].setIcon(null);
            cell[newX][newY].setValue(0);
            cell[newX][newY].setText(Integer.toString(cell[newX][newY].getValue()));
            System.out.println("STEPS=" + getSteps());
            System.out.println("TURNS=" + getTurns());
            return;
        } else {
            fillFlood(newX, newY);
        }
        cell[M / 2][N / 2].setBackground(Color.red);
        cell[M / 2][N / 2].setIcon(null);
        cell[M / 2][N / 2].setValue(0);
        cell[M / 2][N / 2].setText(Integer.toString(cell[M / 2][N / 2].getValue()));

        //System.out.println("RUN FILLFLOOD");
        //  TimeUnit.SECONDS.sleep(1);
    }

    public void fillFloodNew(int x, int y) {

        stack.push(cell[x][y]);

        while (!stack.empty()) {

            CellJlabel cellStack = (CellJlabel) stack.pop();

            int q = cellStack.getCellX();
            int w = cellStack.getCellY();
            int minimum = findMinCell(q, w).getValue();

            if (cellStack.getCellX() == M / 2 && cellStack.getCellY() == N / 2) {
                break;
            }
            if (cellStack.getValue() > M * N) {
                JOptionPane.showMessageDialog(this, "There is no Path to Target");
                removeAll();
                createAndShowGui();
            }

            if (cellStack.getValue() - 1 != minimum) {
                cellStack.setValue(minimum + 1);
                SwingWorker worker = new SwingWorker() {
                    @Override
                    protected Object doInBackground() throws Exception {
                        TimeUnit.MILLISECONDS.sleep(TIME);
                        if (cellStack.getIcon() != null) {
                            cellStack.setBackground(new Color(153, 255, 153));
                        } else {
                            cellStack.setBackground(Color.yellow);
                        }
                        cellStack.setText(Integer.toString(cellStack.getValue()));

                        return null;
                    }

                };
                worker.execute();

                if (stack.size() == M * N) {
                    return;
                }

                if (q + 1 < M && cell[q + 1][w].getValue() != M * N) {
                    stack.push(cell[q + 1][w]);
                }
                if (w + 1 < N && cell[q][w + 1].getValue() != M * N) {
                    stack.push(cell[q][w + 1]);
                }
                if (q - 1 > 0 && cell[q - 1][w].getValue() != M * N) {
                    stack.push(cell[q - 1][w]);
                }
                if (w - 1 > 0 && cell[q][w - 1].getValue() != M * N) {
                    stack.push(cell[q][w - 1]);
                }
            }
        }

        return;
    }

    public void findPath() throws InterruptedException {
        this.setTurns(0);
        this.setSteps(0);

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (cell[i][j].getValue() == M * N) {
                    cell[i][j].setBackground(Color.black);
                }
                if (cell[i][j].getValue() != M * N) {
                    if (i == START_X && j == START_Y) {
                        cell[i][j].setBackground(Color.green);
                    } else if (((i == M / 2 && j == N / 2))) {
                        cell[i][j].setBackground(Color.red);
                    } else {
                        cell[i][j].setBackground(Color.white);
                        cell[i][j].setIcon(null);
                    }
                }
            }
        }
        fillFlood(START_X, START_Y);
    }

    public CellJlabel findMinCell(int x, int y) {

        int m = M * N;
        int newXX = 0;
        int newYY = 0;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && j >= 0 && i < M && j < N && cell[i][j].getValue() != M * N) {
                    if ((i == x - 1 && j == y) || (i == x + 1 && j == y) || (i == x && j == y + 1) || (i == x && j == y - 1)) {
                        if (cell[i][j].getValue() < m) {
                            m = cell[i][j].getValue();
                            newXX = cell[i][j].getCellX();
                            newYY = cell[i][j].getCellY();

                        }
                    }
                }
            }
        }
        return cell[newXX][newYY];

    }

    public int orientation(int xOld, int yOld, int xNew, int yNew) {

        int heading = 0;

        if (xOld < xNew) {
            heading = 1;    //Down
        }
        if (yOld < yNew) {
            heading = 2;    //Right
        }
        if (xOld > xNew) {
            heading = 4;    //Up
        }
        if (yOld > yNew) {
            heading = 3;    //Left
        }

        return heading;
    }

    public void randomMaze() {

        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                Random rand = new Random();
                int n = rand.nextInt((M * N) + M);
                if ((i != START_X || j != START_Y) && (i != M / 2 || j != N / 2) && n > M * N) {
                    cell[i][j].setBackground(Color.black);
                    cell[i][j].setValue(M * N);
                    cell[i][j].setText(Integer.toString(cell[i][j].getValue()));
                }

            }
        }
    }

    private int rand() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void readStack(Stack mystack) {
        for (int i = 0; i < mystack.size(); i++) {
            CellJlabel myCell = (CellJlabel) stack.peek();
            //System.out.println("Element Number i=" + i + "    X=" + myCell.getCellX() + "    Y=" + myCell.getCellY() + "    Value=" + myCell.getValue());
        }
    }

    public void saveMaze() {

        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            // save to file

            try {
                FileOutputStream fileOut = new FileOutputStream(file + ".ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                for (int i = 0; i < M; ++i) {
                    for (int j = 0; j < N; ++j) {
                        out.writeObject(cell[i][j]);
                    }
                }
                out.close();
                fileOut.close();
                //System.out.printf("Serialized data is saved in C:/Users/std84266/Desktop/Maze.ser");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void loadMaze() {

        this.removeAll();
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            // load from file

            try {
                FileInputStream fileIn = new FileInputStream(file);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                for (int i = 0; i < M; ++i) {
                    for (int j = 0; j < N; ++j) {
                        cell[i][j] = (CellJlabel) in.readObject();
                        cell[i][j].setIcon(null);
                        cell[i][j].setText(Integer.toString(cell[i][j].getValue()));
                        this.add(cell[i][j]);
                        addMouseMotionListener(cell[i][j]);
                        addMouseListener(cell[i][j]);
                    }
                }

                in.close();
                fileIn.close();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (ClassNotFoundException c) {
                System.out.println("CellJlabel class not found");
                c.printStackTrace();
                return;
            }
            //System.out.println("Deserialized CellJlabel...");
            repaint();
        }
    }

    public void repaintMaze() {

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (cell[i][j].getValue() == M * N) {
                    cell[i][j].setBackground(Color.black);
                }
                if (cell[i][j].getValue() != M * N) {
                    if (i == START_X && j == START_Y) {
                        cell[i][j].setBackground(Color.green);
                    } else if (((i == M / 2 && j == N / 2))) {
                        cell[i][j].setBackground(Color.red);
                    } else {
                        cell[i][j].setBackground(Color.white);
                        cell[i][j].setIcon(null);
                    }
                }
            }
        }
    }

    public void setTime(int time) {
        TIME = time;
    }

    public int getTime() {
        return TIME;
    }

    public void setSteps(int steps) {
        STEPS = steps;
    }

    public int getSteps() {
        return STEPS;
    }

    public void setTurns(int turns) {
        TURNS = turns;
    }

    public int getTurns() {
        return TURNS;
    }

    public String toString() {
        return ("STEPS=" + getSteps() + "\n" + "TURNS=" + getTurns() + "\n");
    }

    public void setColumns(int columns) {
        N = columns;
    }

    public int getColumns() {
        return N;
    }

    public void setRows(int rows) {
        M = rows;
    }

    public int getRows() {
        return M;
    }
}
