/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptixiaki_jlabel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author std84266
 */
public class ControlPanel extends JPanel {

    private CellJlabel cell;
    private MazeBoardPanel mazeBoard;
    private long time1 = 0;

    public ControlPanel() {
        this.setVisible(true);
        this.setBackground(Color.lightGray);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(415, 0));

        Border mazeDimensionBorder = new CompoundBorder(
                new TitledBorder("Maze Dimensions"),
                new EmptyBorder(4, 60, 4, 60));
        JPanel mazeDimension = new JPanel();
        //mazeDimension.setLayout(new BorderLayout());
        mazeDimension.setBackground(Color.gray);
        mazeDimension.setBounds(5, 5, 405, 100);
        mazeDimension.setBorder(mazeDimensionBorder);
        mazeDimension.setVisible(true);
        this.add(mazeDimension);

        TextField rows = new TextField("33", 15);
        TextField columns = new TextField("33", 15);

        mazeDimension.add(rows);
        mazeDimension.add(columns);

        rows.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mazeBoard.setRows(Integer.parseInt(rows.getText()));
                mazeBoard.removeAll();
                mazeBoard.revalidate();
                mazeBoard.createAndShowGui();
            }
        });

        columns.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mazeBoard.setColumns(Integer.parseInt(columns.getText()));
                mazeBoard.removeAll();
                mazeBoard.revalidate();
                mazeBoard.createAndShowGui();
            }
        });

        /*    JButton solve = new JButton("SOLVE");
        solve.setBounds(180, 400, 90, 30);
        this.add(solve);
        solve.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                try {
                    mazeBoard.fillFlood(START_X, START_Y);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
         */
        JButton reset = new JButton("RESET");
        reset.setBounds(180, 500, 90, 30);
        this.add(reset);
        reset.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mazeBoard.removeAll();
                mazeBoard.createAndShowGui();
            }
        });
        /*
        JButton findPath = new JButton("PATH");
        findPath.setBounds(180, 580, 90, 30);
        this.add(findPath);
        findPath.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                try {
                    long startTime = System.currentTimeMillis();
                    mazeBoard.findPath();
                    long stopTime = System.currentTimeMillis();
                    System.out.println("TIME=" + (stopTime - startTime));
                } catch (InterruptedException ex) {
                    Logger.getLogger(ControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
         */
        JButton createMaze = new JButton("CREATE MAZE");
        createMaze.setBounds(180, 660, 90, 30);
        this.add(createMaze);
        createMaze.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mazeBoard.randomMaze();
            }
        });

        JButton saveMaze = new JButton("SAVE MAZE");
        saveMaze.setBounds(180, 780, 90, 30);
        this.add(saveMaze);
        saveMaze.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mazeBoard.saveMaze();
            }
        });

        JButton loadMaze = new JButton("LOAD MAZE");
        loadMaze.setBounds(180, 880, 90, 30);
        this.add(loadMaze);
        loadMaze.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mazeBoard.loadMaze();
            }
        });

        Border controlBorder = new CompoundBorder(new TitledBorder("Maze Controls"), new EmptyBorder(4, 80, 4, 84));
        JPanel mazeControl = new JPanel();
        mazeControl.setLayout(new GridLayout(3, 2));
        mazeControl.setBackground(Color.gray);
        mazeControl.setBounds(5, 5, 300, 100);
        mazeControl.setBorder(controlBorder);
        mazeControl.setVisible(true);
        this.add(mazeControl);

        // mazeControl.add(solve);
        mazeControl.add(saveMaze);
        mazeControl.add(loadMaze);
        mazeControl.add(createMaze);
        mazeControl.add(reset);

        Border speedBorder = new CompoundBorder(new TitledBorder("Speed Control"), new EmptyBorder(4, 43, 4, 43));
        JPanel speedPanel = new JPanel();
        speedPanel.setBackground(Color.gray);
        speedPanel.setBounds(5, 5, 300, 100);
        speedPanel.setBorder(speedBorder);
        speedPanel.setBackground(Color.gray);
        speedPanel.setVisible(true);
        this.add(speedPanel);

        JSlider speedSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        speedSlider.setSize(new Dimension(100, 200));
        speedSlider.setMinorTickSpacing(2);
        speedSlider.setMajorTickSpacing(10);
        speedSlider.setPaintTicks(true);
        speedSlider.setPaintLabels(true);
        speedSlider.setBackground(Color.gray);
        speedPanel.add(speedSlider);

        JCheckBox checkbox = new JCheckBox("Max Speed");
        checkbox.setBackground(Color.gray);
        checkbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                JCheckBox cb = (JCheckBox) event.getSource();
                if (cb.isSelected()) {
                    mazeBoard.setTime(0);
                    // do something if check box is selected
                } else {
                    mazeBoard.setTime(11000 / speedSlider.getValue());
                }
            }
        });

        speedPanel.add(checkbox);
        speedSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                mazeBoard.setTime(11000 / speedSlider.getValue());
                //   System.out.println("Slider1: " + speedSlider.getValue());
            }
        });

        Border textBorder = new CompoundBorder(new TitledBorder("Console"), new EmptyBorder(4, 15, 4, 15));
        JPanel textPanel = new JPanel();
        //mazeDimension.setLayout(new BorderLayout());
        textPanel.setBackground(Color.gray);
        //textPanel.setBounds(5, 5, 405, 100);
        textPanel.setBorder(textBorder);
        textPanel.setVisible(true);
        this.add(textPanel);

        TextArea console1 = new TextArea();
        console1.setEditable(false);
        console1.setPreferredSize(new Dimension(350, 80));

        textPanel.add(console1);

        Border radioBorder = new CompoundBorder(new TitledBorder("Maze Controls"), new EmptyBorder(4, 59, 4, 59));
        JPanel radioControl = new JPanel();
        //radioControl.setLayout(new GridLayout(3, 2));
        radioControl.setBackground(Color.gray);
        radioControl.setBounds(5, 5, 300, 100);
        radioControl.setBorder(radioBorder);
        radioControl.setVisible(true);
        this.add(radioControl);

        JRadioButton fillflood = new JRadioButton("Fill Flood");
        fillflood.setBackground(Color.gray);
        JRadioButton Treax = new JRadioButton("Treaux");
        Treax.setBackground(Color.gray);
        JRadioButton A = new JRadioButton("A*");
        A.setBackground(Color.gray);

        ButtonGroup group = new ButtonGroup();
        group.add(fillflood);
        group.add(Treax);
        group.add(A);
        setLayout(new FlowLayout());

        JButton btn = new JButton("SOLVE");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                if (fillflood.isSelected()) {
                    try {
                        long startTime = System.currentTimeMillis();
                        mazeBoard.findPath();
                        long stopTime = System.currentTimeMillis();
                        time1 = (stopTime - startTime);
                        System.out.println("TIME=" + (stopTime - startTime));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    console1.setText(mazeBoard.toString() + "TIME=" + time1 + "msec");
                } else if (Treax.isSelected()) {
                    JOptionPane.showMessageDialog(null, "You select : Item 2");
                } else if (A.isSelected()) {
                    JOptionPane.showMessageDialog(null, "You select : Item3");
                } else {
                    JOptionPane.showMessageDialog(null, "You not select.");
                }
            }
        });
        btn.setBounds(125, 154, 89, 23);
        // this.add(btn);

        radioControl.add(btn);
        radioControl.add(fillflood);
        radioControl.add(Treax);
        radioControl.add(A);

        JPanel statistisPanel = new JPanel();
        statistisPanel.setBackground(Color.gray);
        //textPanel.setBounds(5, 5, 405, 100);

        statistisPanel.setVisible(true);
        this.add(statistisPanel);

        BarChart_AWT chart = new BarChart_AWT("Car Usage Statistics",
                "Which car do you like?");

        JFreeChart barChart = ChartFactory.createBarChart(
                "Algorithms Data",
                "Times",
                "Score",
                chart.createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(400, 400));
        statistisPanel.add(chartPanel);

        JButton statistics = new JButton("STATISTICS");
        createMaze.setBounds(180, 660, 90, 30);
        this.add(statistics);
        statistics.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statistisPanel.revalidate();
                statistisPanel.removeAll();
                BarChart_AWT chart = new BarChart_AWT("Car Usage Statistics",
                        "Which car do you like?");
                chart.setTime((int) time1);
                JFreeChart barChart = ChartFactory.createBarChart(
                        "Algorithms Data",
                        "Times",
                        "Score",
                        chart.createDataset(),
                        PlotOrientation.VERTICAL,
                        true, true, false);

                ChartPanel chartPanel = new ChartPanel(barChart);
                chartPanel.setPreferredSize(new java.awt.Dimension(400, 400));
                statistisPanel.add(chartPanel);
                statistisPanel.repaint();
            }
        });

    }

    public void setMazeBoardPanel(MazeBoardPanel m) {
        mazeBoard = m;
    }

    public MazeBoardPanel getMazeBoardPanel() {
        return mazeBoard;
    }

}
