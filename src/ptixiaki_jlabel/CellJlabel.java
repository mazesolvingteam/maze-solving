/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptixiaki_jlabel;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static javax.swing.SwingConstants.CENTER;
import javax.swing.SwingUtilities;
import static ptixiaki_jlabel.MazeBoardPanel.M;
import static ptixiaki_jlabel.MazeBoardPanel.N;
import static ptixiaki_jlabel.MazeBoardPanel.PREF_H;
import static ptixiaki_jlabel.MazeBoardPanel.PREF_W;

public class CellJlabel extends JLabel implements MouseListener, MouseMotionListener {

    private static ImageIcon iconDown = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\down.png");
    private static ImageIcon iconLeft = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\left.png");
    private static ImageIcon iconRight = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\right.png");
    private static ImageIcon iconUp = new ImageIcon("C:\\Users\\std84266\\Documents\\NetBeansProjects\\Ptixiaki_Jlabel_9.4\\src\\ptixiaki_jlabel\\image\\up.png");
    private int Cell_X;
    private int Cell_Y;
    private int Value = 0;
    private boolean Is_Wall = false;
    private int Heading;

    protected Point pointStart = null;
    protected Point pointEnd = null;

    public CellJlabel(int x, int y, int value) {
        Cell_X = x;
        Cell_Y = y;
        Value = value;
        Heading = 0;
        //this.setBackground(Color.white);
        this.setOpaque(true);
        this.setBorder(BorderFactory.createLineBorder(Color.black));
        this.setHorizontalAlignment(CENTER);
        this.setText(Integer.toString(Value));
    }

    public void setValue(int value) {
        Value = value;
    }

    public int getValue() {
        return Value;
    }

    public void setCellX(int x) {
        Cell_X = x;
    }

    public int getCellX() {
        return Cell_X;
    }

    public void setCellY(int y) {
        Cell_Y = y;
    }

    public int getCellY() {
        return Cell_Y;
    }

    public void setHeading(int heading) {
        Heading = heading;
    }

    public int getHeading() {
        return Heading;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int dist = 0;
        if (SwingUtilities.isRightMouseButton(e)) {
            pointStart = e.getPoint();
            if ((int) pointStart.getX() / (PREF_W / M) == (int) Cell_Y && (int) pointStart.getY() / (PREF_H / N) == Cell_X) {
                this.setBackground(Color.white);
                dist = Math.abs(N / 2 - Cell_Y) + Math.abs(M / 2 - Cell_X);
                this.setValue(dist);
                this.setText(Integer.toString(this.getValue()));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        pointEnd = e.getPoint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        pointStart = e.getPoint();
        if ((int) pointStart.getX() / (PREF_W / M) == (int) Cell_Y && (int) pointStart.getY() / (PREF_H / N) == Cell_X) {
            this.setBackground(Color.black);
            this.setOpaque(true);
            this.setValue(M * N);
            this.setText(Integer.toString(this.getValue()));
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    public void repaintCell(CellJlabel c1, CellJlabel c2) throws InterruptedException {

        c1.setBackground(Color.green);
        c2.setBackground(new Color(153, 255, 153));
        c2.setText(Integer.toString(c2.getValue()));
        c1.setText(Integer.toString(c2.getValue()));
        c1.orientation(c1, c2);
        if (((c1.getCellX() == M / 2 && c1.getCellY() == N / 2))) {

            c1.setBackground(Color.red);
            c1.setValue(0);
            c1.setText(Integer.toString(c1.getValue()));
        }
        //  System.out.println("REPAINT CELL");
        //TimeUnit.SECONDS.sleep(2);
    }

    public int orientation(CellJlabel c1, CellJlabel c2) {

        int heading = 0;

        if (c1.getCellX() > c2.getCellX()) {
            heading = 1;    //Down
            c2.setIcon(iconDown);
            // cell[xOld][yOld].setIcon(null);
        }
        if (c1.getCellY() > c2.getCellY()) {
            heading = 2;    //Right
            c2.setIcon(iconRight);
            //  cell[xOld][yOld].setIcon(null);
        }
        if (c1.getCellX() < c2.getCellX()) {
            heading = 4;    //Up
            c2.setIcon(iconUp);
            // cell[xOld][yOld].setIcon(null);
        }
        if (c1.getCellY() < c2.getCellY()) {
            heading = 3;    //Left
            c2.setIcon(iconLeft);
            // cell[xOld][yOld].setIcon(null);
        }

        setHeading(heading);
        return heading;
    }
}
